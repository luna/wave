use Mix.Config

config :wave,
  postgres: [
    hostname: "",
    username: "",
    password: "",
    database: "",

    # if on pgbouncer, uncomment
    # prepare: :unammed
  ],

  # Edit those before starting the server.
  admin_user: %{
    username: "admin",
    password: "admin123",
  }
