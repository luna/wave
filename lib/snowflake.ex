defmodule Snowflake do
  @epoch 1420070400000

  use GenServer
  require Logger

  # client funcs
  def start_link(opts) do
    Logger.info "#{Keyword.get opts, :worker_id}"
    GenServer.start_link(__MODULE__, Keyword.get(opts, :worker_id), opts)
  end

  def make(pid) when is_pid(pid) do
    timestamp = :erlang.system_time(:millisecond)
    make_from_timestamp(pid, timestamp - @epoch)
  end

  def make(worker_id) when is_number(worker_id) do
    case :global.whereis_name "snowflake_#{worker_id}" do
      :undefined -> :worker_not_found
      pid -> make(pid)
    end
  end

  @spec make_from_timestamp(pid(), integer()) :: integer()
  def make_from_timestamp(pid, timestamp) do
    GenServer.call(pid, {:make, timestamp})
  end

  def get_timestamp(snowflake) do
    <<
      timestamp :: unsigned-integer-size(42),
      rest :: unsigned-integer-size(22)
    >> = << snowflake :: unsigned-integer-size(64) >>

    timestamp + @epoch
  end


  # server callbacks
  def init(worker_id) do
    Logger.info "Snowflake: starting worker #{worker_id}"
    :global.register_name "snowflake_#{worker_id}", self()
    {:ok, %{
      worker_id: worker_id,
      generated: 0,
    }}
  end

  def handle_call({:make, timestamp}, _from, state) do
    << snowflake :: unsigned-integer-size(64) >> = <<
      timestamp :: unsigned-integer-size(42),
      state.worker_id :: unsigned-integer-size(10),
      state.generated :: unsigned-integer-size(12),
    >>

    {:reply, snowflake, Map.put(state, :generated, state.generated + 1)}
  end

end
