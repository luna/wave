defmodule Wave.User do
  require Logger

  def add_user(username, password) do
    generated_id = Snowflake.make 1

    pwd_hash = password |> Comeonin.Bcrypt.hashpwsalt

    Wave.DB.conn fn dbpid ->
      case Postgrex.query(dbpid, """
      INSERT INTO users (user_id, username, password_hash)
      VALUES ($1, $2, $3)
      """, [generated_id, username, pwd_hash]) do
        {:ok, res} ->
          Logger.info "Added user #{generated_id} #{username} #{pwd_hash}"
          IO.inspect res
          :ok
        {:error, %Postgrex.Error{postgres: %{code: :unique_violation}}} ->
          {:error, :user_exists}
        {:error, err} ->
          IO.inspect(err)
          {:error, err}
      end
    end

  end


  def check_password(username, given_password) do
    {:ok, %Postgrex.Result{ rows: [[user_id, pwd_hash]] }} = Wave.DB.query("""
    SELECT user_id, password_hash
    FROM users
    WHERE username = $1
    """, [username])

    {Comeonin.Bcrypt.checkpw(given_password, pwd_hash), user_id, pwd_hash}
  end

end
