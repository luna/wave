defmodule Wave.Token do
  @moduledoc """
  Token module.

  This module uses HMAC-SHA256 from erlang's :crypto module
  to sign data.

  This can be used to generate tokens for users.
  """

  @epoch 1293840000

  def raw_sign(data, secret) do
    :crypto.hmac(:sha256, data, secret)
  end

  @spec sign(atom(), String.t, String.t) :: String.t
  def sign(:nontimed, data, secret) do
    hmac = raw_sign(data, secret)
           |> :base64.encode

    "#{data}.#{hmac}"
  end

  def sign(:timed, data, secret) do
    timestamp = :erlang.system_time(:second) - @epoch
                |> to_string
                |> :base64.encode

    actual = "#{data}.#{timestamp}"

    sign(:nontimed, actual, secret)
  end

  # verify functions

  @spec verify(:nontimed, String.t, String.t) :: bool()
  def verify(:nontimed, signed, key) do
    parts = String.split signed, "."

    data = Enum.at(parts, 0)
    hmac = Enum.at(parts, 1)

    gen = raw_sign(data, key) |> :base64.encode

    gen == hmac
  end

  @spec verify(:timed, Stirng.t, String.t, integer()) :: :ok | {:error, atom()}
  def verify(:timed, signed, key, age) do
    current = :erlang.system_time(:second) - @epoch
    parts = String.split signed, "."

    data = Enum.at(parts, 0)
    timestamp = Enum.at(parts, 1)
    hmac = Enum.at(parts, 2)

    gen = raw_sign("#{data}.#{timestamp}", key) |> :base64.encode

    timestamp_num = timestamp |> :base64.decode |> String.to_integer

    cond do
      gen != hmac ->
        {:error, :invalid_hmac}
      current - timestamp_num > age ->
        {:error, :expired}
      true ->
        :ok
    end
  end

end
