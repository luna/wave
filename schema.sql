CREATE TABLE IF NOT EXISTS users (
    user_id bigint,
    username text unique,
    password_hash text,

    PRIMARY KEY (user_id)
);
